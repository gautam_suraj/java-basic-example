package delegation;

class Worker {
    public void bringBill(){
        System.out.println("Worker is bringing bill");
    }
    public void prepareReport(){
        System.out.println("Worker preparing report");
    }
}
