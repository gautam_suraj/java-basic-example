package delegation;



public class Owner {
    public static void main(String[] args) {
        SuperVisor superVisor = new SuperVisor();
        superVisor.prepareReport();
    }
}

