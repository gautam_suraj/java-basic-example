package delegation;



class SuperVisor {
    Worker worker = new Worker();

    public void prepareReport() {
        worker.prepareReport();
    }
}
