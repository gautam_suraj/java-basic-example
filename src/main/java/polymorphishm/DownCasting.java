package polymorphishm;

public class DownCasting {
    public static void main(String[] args) {
        Parent ref;
        ref = new Child1(); //loose coupling
        ref.cry();

         //ref.eat(); //compilation error
        //parent reference cannot access special method

        //using parent reference we can only access inherited and
        //overridden method can be accessed.

        // Specialized method cannot be accessed by parent ref.

        //But can be accessed by performing down casting

        ((Child1)(ref)).eat(); //down casting



        Child1 child1Reference = (Child1) ref;
        child1Reference.eat();

        Parent parentRef = new Child1(); //loose coupling upcasting
    }
}
