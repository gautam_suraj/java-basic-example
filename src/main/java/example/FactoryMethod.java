package example;

public class FactoryMethod { }
interface Chauchau {
    Chauchau getChauchau();
}
interface Biscuit {
    Biscuit getInstance();
}
class Parleg implements Biscuit {
    @Override
    public Biscuit getInstance() {
        return new Parleg();
    }
}

class Rumpum implements Chauchau {
    @Override
    public Chauchau getChauchau(){
        //logic goes here
        return new Rumpum();
    }
}

class WaiWai implements Chauchau{
    @Override
    public Chauchau getChauchau() {
        return new WaiWai();
    }
}

class Retailer {
    public static Chauchau getRumpumChauchau(){
        return new Rumpum().getChauchau();
    }

    public static Chauchau getWaiwaiChauchau(){return new WaiWai().getChauchau();}

    public static Biscuit getBiscuit(){return new Parleg().getInstance();}
}

class Launch {
    public static void main(String[] args){
         Retailer.getRumpumChauchau();
         Retailer.getWaiwaiChauchau();
         Retailer.getBiscuit();
    }
}

