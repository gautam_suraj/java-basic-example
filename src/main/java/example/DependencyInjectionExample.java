package example;

public class DependencyInjectionExample { }


interface Shape{
    void draw();
}


class Circle implements Shape{
    public void draw(){
        System.out.println("circle is drawing");
    }
}
class Traingle implements Shape{
    public void draw(){
        System.out.println("drawing traingle");
    }
}
class Application{
    public static void main(String[] args) {
        /*Shape c = new Circle();
        c.draw();

        Shape t = new Traingle();
        t.draw();*/
        /*Traingle t = new Traingle();
        drawShape(t);*/
        Drawing drawing = new Drawing();
        drawing.getShape().draw();

    }
    public static void drawShape(Shape shape){
        shape.draw();
    }


}

class Drawing{
    private Shape shape;

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public void drawShape(){
        this.shape.draw();
    }
}












class InstanciationFlag{
    Drawing drawing;
    public void instanciate() {
        Traingle t = new Traingle();
        //drawing.setShape(t);
        drawing.drawShape();

    }


}

class InstanciationFootball{
    Drawing drawing;
    public void instanciate() {
        Circle t = new Circle();
        //drawing.setShape(t);
        drawing.drawShape();

    }


}
