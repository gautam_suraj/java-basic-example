package example;

import org.graalvm.compiler.nodes.memory.MemoryCheckpoint;

public class NeedOfSingleTon { }
/*Data class/ object class*/
class UserAccount{
    private long accountNumber;
    private double balance;
    private String accountName;

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
class AccountService{

    public void withdrawMoney(double withDrawMoney){
        /*
        * business logic to withdraw money..
        *
        * */

    }
}
class Singleton {
    // static variable single_instance of type Singleton
    private static Singleton single_instance = null;

    // variable of type String
    public String s;

    // private constructor restricted to this class itself
    private Singleton() {
        s = "Hello I am a string part of Singleton class";
    }

    // static method to create instance of Singleton class
    public static Singleton getInstance() {
        if (single_instance == null)
            single_instance = new Singleton();

        return single_instance;
    }
    public void doSomething(){

    }
}
