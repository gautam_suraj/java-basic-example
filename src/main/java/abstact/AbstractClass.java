package abstact;

public class AbstractClass {

    public static void main(String[] args) {
       Wing wing = new CGWing();
       Human samyog = new Samyog();
       Human nirajan  = new Nirajan();
       calFromName(samyog,"CR");
       calFromName(nirajan,"Former CR");
    }

    public static void  calFromName(Human human,String name){
        human.callingFromName(name);
    }


}


class Samyog extends Human{
    @Override
    void callingFromName(String name) {
        System.out.println("calling from name");
    }

    @Override
    void flip() {
        System.out.println("Samyog is flying ...");
    }
}

class Nirajan extends Human{
    @Override
    void callingFromName(String name) {
        System.out.println("Nirajan calling name = "+ name);
    }

    @Override
    void flip() {
        System.out.println("Nirajan is flying....");
    }
}

abstract class Human extends Wing{
    private String name;

    public String getName() {
        return name;
    }

    abstract void callingFromName(String name);

}

abstract class Wing{
    abstract void flip();
}


/*
* inheritance
* */
class CGWing extends Wing{


    @Override
    void flip() {
        System.out.println("Now Human can attached wing and can fly.");
    }
}




