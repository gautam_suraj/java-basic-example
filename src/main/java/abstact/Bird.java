package abstact;

import inheritance.PassengerPlane;
import inheritance.section.Parent;

abstract public class Bird {

    /*
    * abstract class contains abstaract method
    *
    * */


     abstract public void eat();

     public void flipWings(){
         System.out.println("");
     }

}

abstract class FlyableBird extends Bird{
     abstract public void fly();
}
abstract class RunnableBird extends Bird{
     abstract public void run();
}

abstract class SwimableBird extends Bird{
     abstract public void swim();
}

abstract class Mammel{
     abstract public void giveDirectBirth();
}

class Bulbul extends FlyableBird{

    @Override
    public void fly() {

    }

    @Override
    public void eat() {

    }
}

class Duck extends SwimableBird{
    @Override
    public void swim() {


    }

    @Override
    public void eat() {

    }
}

class Ostrich extends RunnableBird{
    @Override
    public void run() {

    }

    @Override
    public void eat() {

    }
}

class Bat implements Birds, Mammels{
    @Override
    public void fly() {

    }

    @Override
    public void giveDirectBirth() {

    }
}

/*
* interface
* */

interface Birds{
    void fly();
}
interface Mammels{
    void giveDirectBirth();
}

/*
* database access --> database connect
* fetch -->
*
* insert -->
*
*
* */
/*
  SunMicroSystem
* java makes this Database
* */
interface Database{
     int connenctionPool = 100;
     void connect();
}

interface DatabaseOperation{
    void fetch();
    void insert();
    void update();
}

/*

    this class is written by Orcle developer
* */
class Oracle implements Database,DatabaseOperation{
    @Override
    public void connect() {

    }

    @Override
    public void insert() {

    }

    @Override
    public void update() {

    }

    @Override
    public void fetch() {

    }
}

/*
*
* MySql developer
*
* */
class MySql implements Database{
    @Override
    public void connect() {
        System.out.println("Use connection pool " + connenctionPool);
    }

}


interface Parent1 {
    void eat();
    void sleep();
}
interface Parent2 extends Parent1 {
    void walk();
    void rest();
}


class Child implements Parent1, Parent2 {
    @Override
    public void eat() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void rest() {

    }
}

class Demo32{
    public static void main(String[] args) {
        Parent1 child = new Child();
        child.eat();
        isParent1(child);
        isParent2(child);
        if(child instanceof Parent2){
            isParent2((Parent2) child);
        }




    }

    public static boolean isParent2(Parent1 demo13){
        return true;
    }
    public static boolean isParent1(Parent1 demo13){
        return true;
    }
}


