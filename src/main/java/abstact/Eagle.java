package abstact;

public class Eagle extends Bird {
    //@Override
    public void fly() {
        System.out.println("Eagle fly high");
    }

    @Override
    public void eat() {
        System.out.println("Eagle eat creatures.");
    }
}
