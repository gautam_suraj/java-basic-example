package interfaces;

public interface ScientificCalculator extends Calculator{

    float calculateSine(float a);

    public abstract float calculateCosine(float a);

}


/*
* marker interface // indicator interface
* */
interface a{
    
}

class x implements a{

}


/*
* one interface can extends with another interface
* \
* but cannot implement
*
* */
