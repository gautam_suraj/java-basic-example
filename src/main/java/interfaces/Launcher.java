package interfaces;

public class Launcher {

    public static void main(String[] args) {
        Calculator calculator = new CasioCalculator(); //loose coupliing
        //polymorphism
        calculator.add(3,5);
        calculator.subtract(5,3);
        ((CasioCalculator)(calculator)).divide(3,5);

        calculator = new BillingCalculator();
        calculator.add(3,5);

    }
}
