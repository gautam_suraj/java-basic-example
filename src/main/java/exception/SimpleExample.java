package exception;

import inheritance.PassengerPlane;
import inheritance.Plane;

import java.util.Scanner;

public class SimpleExample {
    public static void main(String[] args) throws ArithmeticException{
        System.out.println("Connection established");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number");
        int a = scanner.nextInt();
        System.out.println("Enter second number");
        int b = scanner.nextInt();

        try {

            /*
            *
            * statement that might generate exception
            * */
            int c = a / b;
            System.out.println("The result is "+ c);
        }catch (ArithmeticException e){
            /*
            * statement will be executed when excetption is generated
            * */
            System.out.println("Please enter the correct value of b, the value of b cannot be zero");
            throw e;
        }finally {
          /*
          * wether exception is generated or not finally block will be executed
          *
          *
          * clear all the resources that are allocated by the statement which are in try block
          * */
            System.out.println("finally executed");
        }

        System.out.println("Connection terminated");
    }
}
