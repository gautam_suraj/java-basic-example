package exception;

import java.io.FileNotFoundException;
import java.util.Scanner;



public class RethrowingExceptionDemo {
    Scanner scanner = new Scanner(System.in);
    public void add() throws ArithmeticException{
        System.out.println("Connection establised to add method");
        System.out.println("Enter the numerator");
        int a = scanner.nextInt();
        System.out.println("Enter the denomerator");
        int b = scanner.nextInt();

        try{
            int c = a / b; //exception wont occured
            System.out.println("Result is " + c);
        }
        catch (ArithmeticException e){
            System.out.println("Problem");
            throw e; //object
        }catch (ClassCastException e){
            throw e;
        }catch (Exception e){
            System.out.println("Genral Exception problem");
           // throw new FileNotFoundException();
        }


    }
}


/*
* types of exception
*
*
* 1. CompileTime/Checked Exception  FileNotFoundException
* 2. Runtime Exception ArithmaticException
*
* */
