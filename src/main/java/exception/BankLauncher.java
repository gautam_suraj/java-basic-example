package exception;

public class BankLauncher {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.initiate();
    }
}
