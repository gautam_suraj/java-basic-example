package exception;

import java.util.Scanner;

public class DucklingException {
    public void add() throws ArithmeticException{
        Scanner scanner = new Scanner(System.in);
        System.out.println("Connection establised to add method");
        System.out.println("Enter the numerator");
        int a = scanner.nextInt();
        System.out.println("Enter the denomerator");
        int b = scanner.nextInt();

        int c = a/b;
        System.out.println("Result is "+ c);
        System.out.println("Connection terminated");
    }
}
