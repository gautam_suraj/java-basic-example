package exception;



public class Launch {
    public static void main(String[] args) {
        RethrowingExceptionDemo
                rethrowingExceptionDemo =
                new RethrowingExceptionDemo();
        try{
            rethrowingExceptionDemo.add();
       }catch (ArithmeticException ae){
            System.out.println("Problem occur ");
            throw ae;
        } finally{
            System.out.println("Problem terminated");
        }


    }
}
