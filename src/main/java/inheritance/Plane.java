package inheritance;

public class Plane {
    public void takeOff(){
        System.out.println("A plane is taking out");
    }
    public void fly(){
        System.out.println("A plane is flying");
    }

    public void land(){
        System.out.println("Plane is landing");
    }
}

 interface Plan{
    void takeOff();
    void fly();
    void land();
}

class PP implements Plan{
    @Override
    public void takeOff() {

    }

    @Override
    public void fly() {

    }

    @Override
    public void land() {

    }
}


