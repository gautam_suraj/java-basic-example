package inheritance;

public class CargoPlane extends Plane {

    public void fly() {
        System.out.println("cargo plane is flying");
    }
    public void carryGoods(){
        System.out.println("Carrying goods");
    }
}
