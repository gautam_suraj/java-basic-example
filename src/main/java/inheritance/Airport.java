package inheritance;

public class Airport {
    public static void main(String[] args) {
        PassengerPlane passengerPlane = new PassengerPlane();
        CargoPlane cargoPlane = new CargoPlane();
        FighterPlane fighterPlane = new FighterPlane();
        passengerPlane.carryPassenger();
        passengerPlane.takeOff();
        passengerPlane.fly();


        cargoPlane.takeOff();
        cargoPlane.fly();
        cargoPlane.carryGoods();

        fighterPlane.takeOff();
        fighterPlane.fly();
        fighterPlane.carryArms();
    }
}
