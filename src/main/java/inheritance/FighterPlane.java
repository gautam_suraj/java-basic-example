package inheritance;

public class FighterPlane extends Plane {

    @Override
    public void fly() {
        System.out.println("Fighter plane is flying");
    }

    public void carryArms(){
        System.out.println("Carrying arms and ammunition");
    }
   /*
   * inherited method
   * overriden method
   * spcilized method
   *
   * */
}
