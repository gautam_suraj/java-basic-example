package inheritance.section;



public class Parent {

     protected int x;
     float price;

    protected Parent(float price){
        this.price = price;
    }

    public Parent(){

    }

     protected void add(int i, int j){
        System.out.println("sum is "+i+j);
    }

}

class Child extends Parent{

}

class Simple{
    public static void main(String[] args) {
        int av = new Child().x;
        new Child().add(1,2);
    }
}
