package inheritance;

import serilization.Player;

public class PassengerPlane extends Plane{

        public void fly(){
            System.out.println("A passenger plane is flying");
        }
        public void carryPassenger(){
            System.out.println("Carrying passenger");
        }

}
class SmallPlane extends  PassengerPlane{

}

