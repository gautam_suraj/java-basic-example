public class OverLoadingTypePromotion {
    public static void main(String[] args) {
        Add add = new Add();
        int a = 5;
        int b = 10;
        float c = add.sum(a,b);
        //sum(int,int) will go to sum(int,float)
        //check the first parameter and
        //the close parameter for the second type is checked.
        System.out.println("sum of a and b "+ c);


        SecondAdd secondAdd = new SecondAdd();
        //error due to ambiguity
        //secondAdd.sum(a,b);
    }
}

class Add{
    public float sum(int a, float b){
        return a+b;
    }
    public double sum(float a, double c){
        return a+c;
    }
}

class SecondAdd{
    public float sum(int a, float b){
        return a +b;
    }
    public float sum(float a, int b){
        return a +b;
    }
}