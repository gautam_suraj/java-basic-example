package multithreading;

public class DeadLockProblem {
    public static void main(String[] args) {
        Student student1 = new Student();
        Student student2 = new Student();
        student1.setName("Shyam");
        student2.setName("Ram");
        student1.start();
        student2.start();
    }

}
class Student extends Thread{
    String res1 = "copy";
    String res2 = "pen";
    String res3 = "table";
    public void run(){
        if(getName().equals("Shyam")){
           shyamWriting();
        }else{
             ramWriting();
        }
    }
    public void shyamWriting(){
        try{
            Thread.sleep(5000);
            synchronized (res1){
                System.out.println("Shyam acquired " + res1);
                Thread.sleep(5000);
                synchronized (res2){
                    System.out.println("Shyam acquired "+ res2);
                    Thread.sleep(5000);
                    synchronized (res3){
                        System.out.println("Shyam acquired "+ res3);
                    }
                }
            }
        }catch (Exception e){

        }
    }
    /*
    * only one thread can execute this
    * */
    synchronized public void abc(){

    }
    public void ramWriting(){
        try{
            Thread.sleep(5000);
            synchronized (res3){
                System.out.println("Ram acquired " + res3);
                Thread.sleep(5000);
                synchronized (res2){
                    System.out.println("Ram acquired "+ res2);
                    Thread.sleep(5000);
                    synchronized (res1){
                        System.out.println("Ram acquired "+ res1);
                    }
                }
            }
        }catch (Exception e){

        }
    }
}
