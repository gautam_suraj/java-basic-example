package multithreading;

import java.util.Scanner;

public class TheradRunnableExample {
    public static void main(String[] args) {
        System.out.println("Main Thread started");
        Demo1A demo1A = new Demo1A();
        Demo1B demo1B = new Demo1B();
        Demo1C demo1C = new Demo1C();
        Thread t1 = new Thread(demo1A);
        Thread t2 = new Thread(demo1B);
        Thread t3 = new Thread(demo1C);
        t1.start();
        t2.start();
        t3.start();
        System.out.println("MainThread terminated");
    }
}

/*
*
* */
class Demo1A implements Runnable{
    @Override
    public void run() {
        System.out.println("Banking started");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter account and password");
        int ac = scanner.nextInt();
        int pw = scanner.nextInt();
        System.out.println("Banking terminated");
    }
}
class Demo1B implements Runnable{
    @Override
    public void run() {
        try{
            System.out.println("Printing started");
            for(int i = 1;i<=5;i++){
                System.out.println("ABC");
                Thread.sleep(5000L);
            }
        }catch(Exception e){
            System.out.println("Problem in printing");
        }
        System.out.println("Printing completed");
    }
}
class Demo1C implements Runnable{
    @Override
    public void run() {
        try{
            System.out.println("Adding started");
            int a = 56789;
            int b = 88895;
            Thread.sleep(3000);
            int c = a+b;
            System.out.println("sum = "+ c);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Adding done...");

    }
}

