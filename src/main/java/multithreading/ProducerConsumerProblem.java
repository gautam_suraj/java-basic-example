package multithreading;

public class ProducerConsumerProblem {
    public static void main(String[] args) {
        Queue q = new Queue();
        Producer producer = new Producer(q);
        Consumer consumer = new Consumer(q);
        producer.start();
        consumer.start();
    }
}

class Queue{
    /*int x;
    *//*
    * producer
    *
    * *//*
    public void put(int i){
        x = i;
        System.out.println("Putting x = "+ i);
    }

    *//*
    *
    * consumer
    * *//*
    public void get(){
        System.out.println("Getting x = "+ x);
    }*/

    int x;
    boolean value_is_present_in_x = false;  //flag

    /*
    * producer thread only call this method
    * */
    synchronized public void put(int i){
        try{
            if(value_is_present_in_x){  // checking if container is empty or not
                /*
                * container is not empty
                * */
                wait();  // producer thread goes to wait state.
            }else{
                /*
                * case while container is empty
                * */
                x = i;
                System.out.println("Putting x = "+ i);
                value_is_present_in_x = true;
                notify();  //if consumer is waiting then tell consumer to awake
            }
        }catch (Exception e){
            System.out.println("Interruption occoured");
        }
    }

    synchronized public void get(){
        try{
            if(value_is_present_in_x == false){
                /*
                * if container is empty
                *
                * */
                wait();
            }else{
                /*
                * container is full
                * */
                System.out.println("Getting x = "+ x);
                value_is_present_in_x = false;
                notify();
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}

class Producer extends Thread{
    Queue a;

    Producer(Queue a){
        this.a = a;
    }

    public void run(){
        int i = 1;
        while(true){   //loop type infinite loop
            try {
                Thread.sleep(2000);
                a.put(i++);
            }catch (InterruptedException e){

            }
        }
    }
}
class Consumer extends Thread{
    Queue b;

    Consumer(Queue b){
        this.b = b;
    }

    public void run(){
        while(true){  //infinite loop
            try {
                Thread.sleep(2000);
                b.get();
            }catch (Exception e){

            }
        }
    }
}
