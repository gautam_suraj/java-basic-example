package multithreading;

public class SimpleMultiThreading {
    public static void main(String[] args) {
        Thread newThread = new Thread(); //thread's object
        System.out.println(newThread);

        Thread t = Thread.currentThread();
        System.out.println(t);
        System.out.println(t.getName());
        System.out.println(t.getPriority());

        /*thread name
        * thread priority
        * method name executed by the Thread
        *
        * */
        t.setName("My Thread");
        t.setPriority(1);
        System.out.println(t);
    }
}
