package multithreading;

public class SolvedRaceCondition {
    public static void main(String[] args) {
        MSWords m1 = new MSWords();
        MSWords m2 = new MSWords();
        MSWords m3 = new MSWords();

        m1.setName("TYPE");
        m2.setName("SPELL_CHECK");
        m3.setName("AUTO_SAVE");
        m2.setDaemon(true);
        m3.setDaemon(true);
        m2.setPriority(9);
        m3.setPriority(10);
        m1.start();
        m2.start();
        m3.start();
    }
}
class MSWords extends Thread{
    @Override
    public void run() {
        if(getName().equals("TYPE")){
            typing();
        }else if(getName().equals("SPELL_CHECK")){
            spellChecking();
        }else{
            autoSaving();
        }
    }
    public void typing(){
        try{
            for ( ; ; ){
                System.out.println("Typing");
                Thread.sleep(5000);
            }
        }catch (Exception e){

        }
    }
    public void autoSaving(){
        try{
            for (;;){
                System.out.println("Autosaving");
                Thread.sleep(5000);
            }
        }catch (Exception e){

        }
    }
    public void spellChecking(){
        try{
            for (; ;){
                System.out.println("SpellChecking");
                Thread.sleep(5000);
            }
        }catch (Exception e){

        }
    }
}
