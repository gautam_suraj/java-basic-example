package multithreading;

public class RaceCondition {
    public static void main(String[] args) {
        MSWord m1 = new MSWord();
        MSWord m2 = new MSWord();
        MSWord m3 = new MSWord();

        m1.setName("TYPE");
        m2.setName("SPELL_CHECK");
        m3.setName("AUTO_SAVE");
        m1.start();
        m2.start();
        m3.start();
    }
}
class MSWord extends Thread{
    @Override
    public void run() {
        if(getName().equals("TYPE")){
            typing();
        }else if(getName().equals("SPELL_CHECK")){
            spellChecking();
        }else{
            autoSaving();
        }
    }
    public void typing(){
        try{
            for (int i = 0; i<=3; i++){
                System.out.println("Typing");
                Thread.sleep(5000);
            }
        }catch (Exception e){

        }
    }
    public void autoSaving(){
        try{
            for (int i=0; i<=3; i++){
                System.out.println("Autosaving");
                Thread.sleep(5000);
            }
        }catch (Exception e){

        }
    }
    public void spellChecking(){
        try{
            for (int i = 0; i<=3; i++){
                System.out.println("SpellChecking");
                Thread.sleep(5000);
            }
        }catch (Exception e){

        }
    }
}
