package multithreading;

import java.util.Scanner;


class Demo1 extends Thread{
    @Override
    public void run() {
        System.out.println("Banking started");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter account and password");
        int ac = scanner.nextInt();
        int pw = scanner.nextInt();
        System.out.println("Banking terminated");
    }
}
class Demo2 extends Thread{
    @Override
    public void run() {
        try{
            System.out.println("Printing started");
            for(int i = 1;i<=5;i++){
                System.out.println("ABC");
                Thread.sleep(5000);
            }
        }catch(Exception e){
            System.out.println("Problem in printing");
        }
        System.out.println("Printing completed");
    }
}
class Demo3 extends Thread{
    @Override
    public void run() {
       try{
           System.out.println("Adding started");
           int a = 56789;
           int b = 88895;
           Thread.sleep(3000);
           int c = a+b;
           System.out.println("sum = "+ c);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
        System.out.println("Adding done...");

       }
}
public class ThreadExendsExample {


    public static void main(String[] args) {
        Demo1 demo1 = new Demo1();
        Demo2 demo2 = new Demo2();
        Demo3 demo3 = new Demo3();
        demo1.start();
        demo2.start();
        demo3.start();
        System.out.println("Main therad completed");
    }
}
