package multithreading;

public class ProducerConsumerInterThreadCommunication {

}

class ThreadSafeQueue{
    int x;
    boolean value_is_present_in_x = false;

    synchronized public void put(int i){
        try{
            if(value_is_present_in_x){
                wait();
            }else{
                System.out.println("Putting x = "+ i);
                x = i;
                value_is_present_in_x = true;
                notify();
            }
        }catch (Exception e){
            System.out.println("Interruption occoured");
        }
    }

    synchronized public void get(){
        try{
            if(!value_is_present_in_x){
                wait();
            }else{
                System.out.println("Getting x = "+ x);
                value_is_present_in_x = false;
                notify();
            }
        }catch (Exception e){

        }
    }
}

/*
* for wait and try synchronized is necessary
*
* */
