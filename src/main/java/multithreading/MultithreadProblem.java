package multithreading;

import java.util.concurrent.Semaphore;

class MultithredProblem {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1);
        Bathroom bathroom = new Bathroom(semaphore);
        Thread ram = new Thread(bathroom);
        Thread sita = new Thread(bathroom);
        Thread hari = new Thread(bathroom);


        /*Thread ram = new BathroomResource();
        Thread sita = new BathroomResource();
        Thread hari = new BathroomResource();*/

        ram.setName("Ram");
        sita.setName("Sita");
        hari.setName("Hari");

        ram.start();
        sita.start();
        hari.start();
    }
}

class BathroomResource extends Thread{
    @Override
    public void run() {
        try {
            System.out.println(Thread.currentThread().getName() + " has entered bathroom");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + " using bathroom");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + " exit from bathroom");
            Thread.sleep(2000);
        }catch (Exception e){

        }
    }
}

class Bathroom implements Runnable {
    private Semaphore semaphore;
      Bathroom(Semaphore semaphore) {
        this.semaphore = semaphore;
      }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println(Thread.currentThread().getName() + " has entered bathroom");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + " using bathroom");
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getName() + " exit from bathroom");
            Thread.sleep(2000);
            semaphore.release();
        } catch (Exception e) {
           //e.printStackTrace();
        }
    }
}
/*
* Differs from deadlock
*
* */

