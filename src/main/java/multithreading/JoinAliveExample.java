package multithreading;

public class JoinAliveExample {
    public static void main(String[] args) {
        System.out.println("Main thread started and initlized resources");
        /*creating thread stack*/
        Demo1 demo1 = new Demo1();
        Demo2 demo2 = new Demo2();
        Demo3 demo3 = new Demo3();

        //telling TheadSchedular
        demo1.start();
        demo2.start();
        demo3.start();

        System.out.println("Demo1 isalive " + demo1.isAlive());
        System.out.println("Demo2 isalive " + demo2.isAlive());
        System.out.println("Demo3 isalive "+ demo3.isAlive());

        try {
            demo1.join();
            demo2.join();
            demo3.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Demo1 isalive " + demo1.isAlive());
        System.out.println("Demo2 isalive " + demo2.isAlive());
        System.out.println("Demo3 isalive "+ demo3.isAlive());

        System.out.println("Main Thread completed and deallocates");

    }
}
