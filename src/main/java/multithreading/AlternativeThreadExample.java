package multithreading;

import java.util.Scanner;

/*
* bank --> acc, password
* printing -->
* adding -->
*
*
* */



public class AlternativeThreadExample {
    public static void main(String[] args) {
         Demo demo1 = new Demo();
         Demo demo2 = new Demo();
         Demo demo3 = new Demo();
         demo1.setName("BANK");
         demo2.setName("PRINT");
         demo3.setName("ADD");
        demo1.start();
        demo2.start();
        demo3.start();
    }

}
class Demo extends Thread{
    @Override
    public void run(){
        if(getName().equals("BANK")){
             banking();
        }else if(getName().equals("PRINT")){
             printing();
        }else{
            adding();
        }
    }
    public void banking(){
        System.out.println("Banking started");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter account and password");
        int ac = scanner.nextInt();
        int pw = scanner.nextInt();
        System.out.println("Banking terminated");
    }

    public void printing(){
        try{
            System.out.println("Printing started");
            for(int i = 1;i<=5;i++){
                System.out.println("ABC");
                Thread.sleep(5000L);  //static method //forcefully pausing
            }
        }catch(Exception e){
            System.out.println("Problem in printing");
        }
        System.out.println("Printing completed");
    }
    public void adding(){
        try{
            System.out.println("Adding started");
            int a = 56789;
            int b = 88895;
            Thread.sleep(3000);
            int c = a+b;
            System.out.println("sum = "+ c);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Adding done...");
    }
}


class Banking extends Thread{
    @Override
    public void run() {
        System.out.println("Banking started");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter account and password");
        int ac = scanner.nextInt();
        int pw = scanner.nextInt();
        System.out.println("Banking terminated");
    }
}

class Printing extends Thread{
    @Override
    public void run() {
        try{
            System.out.println("Printing started");
            for(int i = 1;i<=5;i++){
                System.out.println("ABC");
                Thread.sleep(5000L);  //static method //forcefully pausing
            }
        }catch(Exception e){
            System.out.println("Problem in printing");
        }
        System.out.println("Printing completed");
    }
}

class Adding extends Thread{
    @Override
    public void run() {
        try{
            System.out.println("Adding started");
            int a = 56789;
            int b = 88895;
            Thread.sleep(3000);
            int c = a+b;
            System.out.println("sum = "+ c);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Adding done...");
    }
}

class Launcher{
    public static void main(String[] args) {
        Banking banking = new Banking();
        Printing printing = new Printing();
        Adding adding = new Adding();
        banking.start();
        printing.start();
        adding.start();
        abc();
        System.out.println("Main method terminated");
    }
   public static void abc(){
       System.out.println("abc");
    }

}
