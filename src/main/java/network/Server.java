package network;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(4000); // address
            //port reserved port
            //80 --> default port
            //mysql --> xmpp(80) --> mysql(3306)
            System.out.println("Server ready..");
            Socket serverListener = serverSocket.accept();

            //read
            InputStream inputStream = serverListener.getInputStream(); //medium/pipe
            DataInputStream dis = new DataInputStream(inputStream); //data

            //write
            OutputStream outputStream = serverListener.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);



            for (; ;) {
                //reading client request
                String s = dis.readUTF();
                System.out.println(s);


                //sending response to client
                String serverCmd = new Scanner(System.in).nextLine();
                dataOutputStream.writeUTF(serverCmd);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    * java added 3 technologies
    * 1. servlet
    * 2. jsp
    * 3. jdbc
    *
    *
    *
    * */

    /*
    * front end
    *  1. applets
    *  2. swing
    *
    *   //html(mark up language)
    *    css(//butify )
    *   // js(program/scripting language) // (front end)
    *
    * */
}
//JDBC-ODBC bridge driver
//RAM
//[Java app] --> [JDBC] ---C++-->[ODBC] --(mysql query) --------------> [MySql]

//mySql ------------data-------> [ODBC]-->JDBC---[Java app]

//from java we can access data present in database

//dependency mananagement tools