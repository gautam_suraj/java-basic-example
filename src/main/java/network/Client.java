package network;


import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {

        try {
            Socket client = new Socket("127.0.0.1",4000); //location

            //requesting to server i.e write
            OutputStream oos = client.getOutputStream();  //medium
            DataOutputStream dos = new DataOutputStream(oos);

            //read server response
            InputStream inputStream = client.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(inputStream);

            System.out.println("Request server ...");
            for (; ;) {
                //sending request to server

                Scanner scanner = new Scanner(System.in);
                String s = scanner.nextLine();
                dos.writeUTF(s);

                //reading server response
                String response = dataInputStream.readUTF();
                System.out.println(response);
            }






        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
