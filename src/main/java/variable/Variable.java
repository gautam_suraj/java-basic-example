package variable;

public class Variable {
      Dog dog;//Instance variable sets to default value
       int y;
       int z;
       int x = (y + 3)/ z;


    public static int abc;
    //static block
    static {

        System.out.println("variable static block");
        abc = 20;
    }


    public static void main(String[] args) {
        System.out.println("Main method....");
        System.out.println("total dog "+ Dog.totalDog);

        Dog d = new Dog();// local variable   //doesnot set to default value
        Dog.totalDog += 1;
        d.name = "Tommy";
        d.cost = 1200;
        d.breed = "Bulldog";
        d.eat();

        System.out.println("total dog "+ Dog.totalDog);
        Dog d1 = new Dog();// local variable   //doesnot set to default value
        Dog.totalDog += 1;
        d1.name = "Heaven";
        d1.cost = 1200;
        d1.breed = "Pug";
        System.out.println("total dog "+ Dog.totalDog);
        d1.eat();
        System.out.println(d1.name);


    }

    void xxx(){
       int x = (y + 3)/ z;
    }


}

class Dog{


    /*
    * instance variable
    * store in heap segment
    * set to  default value
    * */
     String name;
     String breed;
     int cost;
     static int totalDog;

     /*
     * static block
     * initilized static variable
     * */
    /* static {
         System.out.println("static block...");
         totalDog = 2;
     }*/


     void eat(){
         int totalThings;
         System.out.println("dog is eating");

     }
     void sleep(){

     }
      void bark(){

     }

     static int getAverageLifeSpan(){
         return 0;
     }

}


/*
*
* stack segment
* |
*
*
*
*
*
*
*
* */


class ABC{
    int a;
    int b;
    float c;

    public void add(){
        int x = 0 ;
        int y = 0;
        c = a + b;
        System.out.println(c);
        System.out.println(x + " " + y);
        int abc = XYZ.getX();
        long s  = new XYZ().getL();
        new XYZ().eat();
        XYZ.main(new String[2]);
    }


    public void subtract(){
        int p = 0 ;
        int q = 0;
        c = a + b;
        System.out.println(c);
        System.out.println(p + " " + q);
    }



}

class XYZ{

     private static int x= 2 ;
     private int parrort;
     private long l = 5;
     private char c;

     public static void setX(int a){
         if(a > 0) {
             x = a;
         }
     }

     public static int getX(){
         return x;
     }

     public long getL(){
         return l;
     }

    static {
        x = 20;
    }

    public static void main(String[] args) {
      ABC abc = new ABC();
      abc.add();
      abc.subtract();
      System.out.println( " value of char .." + new XYZ().c);

    }
    public void eat(){

    }

    static public float convertToCM(float meter){
        float xyz = 0.0f;
        return meter * 100;
    }
}



