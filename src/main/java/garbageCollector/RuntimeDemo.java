package garbageCollector;

import java.util.Date;
/*
* Java program Communicating with JVM by using runtime object
*
* */
public class RuntimeDemo {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        /*
        * returns no. of bytes total memory present in the heap
        * */
        System.out.println("Total memory "+ runtime.totalMemory());
        /*
         * returns no. of bytes free memory present in the heap
         * */
        System.out.println("free memory "+runtime.freeMemory());
        for(int i = 0; i<=100; i++){
            Date d = new Date();
            d = null;
        }
        System.out.println("free memory before calling GC"+ runtime.freeMemory());
        runtime.gc(); // System.gc(); //requesting JVM to run GC
        System.out.println("free memory after calling GC"+ runtime.freeMemory());
    }
}
