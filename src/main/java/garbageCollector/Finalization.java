package garbageCollector;
/*
* calls finalize method of such object which is eligible for GC
*
* */
public class Finalization {
    public static void main(String[] args) {
        String s = new String();
        s = null;
        System.gc();  //requesting gc
        System.out.println("End of main");
    }


    public void finalize()  {
        System.out.println("finalize ");
    }
}
/*
* When GC call finalize then only object is destroyed
*
* */
class Test{
    public static void main(String[] args) throws Throwable{
        Test test = new Test();
        /*test.finalize();
        test.finalize();*/
        test = null;
        System.gc();
        System.out.println("End of main....");
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("test finalize...");
    }
}
/*
* Object is eligible for 2 times but GC calls finalize only once
*
*
* */
class MyClass{
    static MyClass myClass;
    public static void main(String[] args) throws Exception{
        MyClass myClass1 = new MyClass();
        System.out.println("hash code of "  + myClass1.hashCode());
        myClass1 = null;
        System.gc();
        Thread.sleep(5000);
        System.out.println(myClass.hashCode()); // throws null pointer exception
        myClass = null;
        Thread.sleep(1000);
        System.out.println("End of main");
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize method is called....");
        myClass = this;
    }
}
