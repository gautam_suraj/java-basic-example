package garbageCollector;

public class EligibilityOfGarbargeCollector {

    public static void main(String[] args) {
        createObject();
        Student ramesh = new Student();

    }

    static void createObject(){
        Student s  = new Student();
        System.out.println(s);
    }
}

/*
* stack `                      heap(Object store in a heap memory)
*   Student s ----------        student[name=null,rollNumber=0,DOB=0L,address=null]
*                       ->      student[name=null,rollNumber=0,DOB=0L,address=null]
*
* */

class Student{
    private String name;
    private int rollNumber;
    private long DOB;
    private String address;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", rollNumber=" + rollNumber +
                ", DOB=" + DOB +
                ", address='" + address + '\'' +
                '}';
    }
}


/*
 * stack `                      heap(Object store in a heap memory)
 *   Test1 test1   ---------->        test1[i=]
 *   Test2 test2==null                test1[i=null]
 *
 * */

class Test1{
    Test1 i;

    public static void main(String[] args) {
        Test1 test1 = new Test1();
        Test1 test2 = new Test1();
        test1.i = test2;
        test2 = null;

        /*  46 -> object

        * is test1 eligible for GC
        *
        * */

    }
}
