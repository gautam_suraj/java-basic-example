package oops;

import inheritance.Plane;

public class DataEncapsulation {
    public static void main(String[] args) {
        Book book = new Book();
        book.setData(100);
        System.out.println(book.getData());
    }
}

class Book{
    private int pageNumber;

    public int getData() {
        return pageNumber;
    }

    /*
    *
    * protecting direct access
    * */
    public void setData(int pageNumber) {
        if(pageNumber > 0) {
            this.pageNumber = pageNumber;
        }else{
            System.out.println("Invalid input");
            System.exit(0);
        }
    }
}


