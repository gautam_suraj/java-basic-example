package oops;

public class ConstructorExample {
    public static void main(String[] args) {
        // Dog dog1 does not have suitable constructor
        //Java does not automatically include zero parameterized constructor
        //because class already has a constructor
        //In such case Java programmer itself has to include default constructor

        // Dog dog1 = new Dog();

        Dog dog = new Dog("Tommy","Bulldog",1000);

        System.out.println("Name of the dog " + dog.getName());
        System.out.println("Breed of the dog " + dog.getBreed());
        System.out.println("Cost of the dog " + dog.getCost());
    }


}

class Dog{
    private String name;
    private String breed;
    private int cost;

    public Dog(String name, String breed, int cost) {
        this(name);
        this.name = name;
        this.breed = breed;
        this.cost = cost;


    }


    public Dog(){
        //this("Tommy", "Bulldog", 1000);
        this.name = "Tommy";
        this.breed = "Bulldog";
        this.cost = 1000;
    }

    public Dog(String name){
        this.name = name;
    }



    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public int getCost() {
        return cost;
    }
}

