package aggregation.composition;

public class Student {

    private Brain brain = new Brain(12);
    private Heart heart = new Heart(72,20);

    public void readBook(Book book){
        System.out.println("Student is reading "+ book.getName());
        System.out.println("student completed reading " + book.getTotalPages() + " pages.");
    }

    public void rideBike(Bike bike){
        System.out.println("Student riding " + bike.getBrand()  + " bike.");
        System.out.println("Studnet riding bike has mileage of "+ bike.getMilage());
    }

    public Brain getBrain() {
        return brain;
    }

    public Heart getHeart() {
        return heart;
    }
}
