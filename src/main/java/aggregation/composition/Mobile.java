package aggregation.composition;

class Mobile {
    private OS os = new OS("Android", 52);

    public Mobile(OS os){
        this.os = os;
    }

    public Mobile(){

    }

    public OS getOs() {
        return os;
    }

    public void chargeMobileFromCharger(Charger charger) {
        System.out.println("Brand of a charger is" + charger.getBrand());
        System.out.println("Color of a charger is" + charger.getColor());
    }
}
