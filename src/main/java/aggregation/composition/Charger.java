package aggregation.composition;

public class Charger {
    private String brand;
    private String color;

    public Charger(String brand, String color) {
        this.brand = brand;
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }
}
