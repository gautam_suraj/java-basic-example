package aggregation.composition;

public class School {
    public static void main(String[] args) {
        /*
        * 2 objects will automatically created.
        * brain and heart
        * */
        Student student = new Student();
        System.out.println("brain weight of student "+ student.getBrain().getWeight());
        System.out.println("heart beat of a student "+ student.getHeart().getHeartBeat());


        Book book = new Book("Science ",400);
        Bike bike = new Bike("Hero Honda",150);

        student.readBook(book);
        student.rideBike(bike);

        System.out.println("Student is missing.....");

        student = null;
        //System.out.println("brain weight of student "+ student.getBrain().getWeight());
        //System.out.println("heart beat of a student "+ student.getHeart().getHeartBeat());


        System.out.println("Is book available " + book.getName());
        //student.readBook(book);


        System.out.println("Is bike available "+ bike.getMilage());
        //student.rideBike(bike);
    }
}
