package aggregation.composition;

public class Heart {
    private int heartBeat;
    private int weight;

    public int getHeartBeat() {
        return heartBeat;
    }

    public int getWeight() {
        return weight;
    }

    public Heart(int heartBeat, int weight) {
        this.heartBeat = heartBeat;
        this.weight = weight;
    }

}
