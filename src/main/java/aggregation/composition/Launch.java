package aggregation.composition;



public class Launch {
    public static void main(String[] args) {
        Mobile mobile = new Mobile();
        Charger charger = new Charger("Samsung", "white");
        System.out.println("OS name " + mobile.getOs().getName());
        System.out.println("OS size " + mobile.getOs().getSize());
        mobile.chargeMobileFromCharger(charger);

        mobile = null;
        //throw null pointer exception
        // System.out.println("Get name of a mobile "+ mobile.getOs().getName());
        // System.out.println("Get size of a mobile "+ mobile.getOs().getSize());

        System.out.println("Getting brand of a charger " + charger.getBrand());
        System.out.println("Getting color of a charger " + charger.getColor());
    }
}

/*
* Hiding refers to the process of programming the object in such a way that
* the other objects cannot directly access the most important component of the
* current object.
*
* Though direct access is prevented through control access is permitted. i.e Encapsulation
*
*
* */

