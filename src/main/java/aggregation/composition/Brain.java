package aggregation.composition;

public class Brain {
    private int weight;

    public Brain(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
