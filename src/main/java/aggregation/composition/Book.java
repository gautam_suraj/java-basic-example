package aggregation.composition;

public class Book {
    private String name;
    private int totalPages;

    public Book(String name, int totalPages) {
        this.name = name;
        this.totalPages = totalPages;
    }

    public String getName() {
        return name;
    }

    public int getTotalPages() {
        return totalPages;
    }
}
