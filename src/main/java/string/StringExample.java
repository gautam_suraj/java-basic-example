package string;

/*
* any class present in java.lang packager then no need to import
* */
import java.util.StringTokenizer;

public class StringExample {

    public static void main(String[] args) {
        //Immutable //unchangeable //constant pool // 1 obj

        /*
        * without new keyword we can crate object get stored in constant pool
        * */
        String s = "ram"; //new object create
        String newS = "ram";

        //non constant pool //2 obj
        String s1 = new String("ram");
        String newS1 = new String("ram");



        //Mutable
        String s2 = new StringBuilder().append("ram").toString();

    }
}

class Demo1{
    /*
    * refrence variable // handler
    *
    * object --> heap segment store
    * handler --> stack segment
    *
    * s1 is storing address of "RAM" --> object  -->Ram is in heap segment
    * */
    public static void main(String[] args) {
        //constant pool
        // 1 obj
        String s1 = "RAM"; //2000
        String s2 = "RAM"; //2000
   // == s1's address is equal s2's address

        if(s1==s2){
            System.out.println("Reference are equals.");
        }else {
            System.out.println("Reference are not equals");

        }
    }
}

class Demo2{
    public static void main(String[] args) {
        String s1 = "RAM";
        String s2 = "RAM";
        if(s1.equals(s2)){
            System.out.println("Strings content are equals.");
        }else{
            System.out.println("Strings contents are not equals");
        }
    }
}

class Demo3{
    public static void main(String[] args) {
        /*
        * non constant pool
        * */
        String s1 = new String("RAM");  // non constant pool //3000
        String s2 = new String("RAM");  //4000
        if(s1 == s2){
            System.out.println("References are equals");
        }else{
            System.out.println("Refrences are not equals");
        }
    }
}

class Demo4{
    public static void main(String[] args) {
        String s1 = new String("RAM");
        String s2 = new String("RAM");
        if(s1.equals(s2)){
            System.out.println("Contents are equals");
        }else{
            System.out.println("Contents are not equals");
        }
    }
}


class Demo5{
    public static void main(String[] args) {
        String s1 = "RAM"; //constant pool -- no duplicate
        String s2 = new String("RAM"); //non constant pool
        if(s1 == s2){
            System.out.println("References are equals");
        }else{
            System.out.println("References are not equals");
        }
    }
}

class Demo6{
    public static void main(String[] args) {
        String s1 = "RAM";
        String s2 = new String("RAM");
        if(s1.equals(s2)){
            System.out.println("String's contents are equals");
        }else{
            System.out.println("String's contents are not equals");
        }
    }
}
/*
*
* comparing case(upper case-->capital ,lower case /small) and content.
* */
class Demo7{
    public static void main(String[] args) {
        String s1 = "ram";
        String s2 = new String("RAM");
        if(s1.equals(s2)){
            System.out.println("String's content are equals");
        }else{
            System.out.println("String's content are not equals");
        }
    }
}

class Demo8{
    public static void main(String[] args) {
        String s1 = "ram";
        String s2 = new String("RAM");
        if(s1.equalsIgnoreCase(s2)){
            System.out.println("String's content are equals");
        }else{
            System.out.println("String's content are not equals");
        }
    }
}



/*
*
* concat --> add, +
* */

/*
* string s = "A"
* string d = "V"
*
* s+d => AV
*
* s.contact(d)  => s = s+d;
*
* s = AV
*
* *
* */

class Demo9{
    public static void main(String[] args) {
        String s1 = new String("RAM");
        //s1 is immutable string -- > once created cannot be changed
        System.out.println(s1);  //RAM
        String s2 = s1.concat("SITA");
        System.out.println(s2); //RAMSITA --> RAM
    }
}




class Demo10{
    public static void main(String[] args) {
        String s1 = "RAM" + "SITA"; // constant pool
        String s2 = "RAM" + "SITA";  // constant pool
        if(s1 == s2){
            System.out.println("Reference are equals");
        }else{
            System.out.println("Reference are not equals");
        }
    }
}

/*
* operator, operand, expression
*
* */
class Demo11{
    public static void main(String[] args) {
        String s1 = "RAM"; // constant pool 1
        String s2 = "SITA"; //constant pool 2
        String s3 = s1 + s2; // --> expression --> non constant pool
        String s4 = s1 + s2; // --> expression  --> non constant pool
        if(s3 == s4){
            System.out.println("Reference are equals");
        }else{
            System.out.println("Reference are not equals");
        }
    }
}

class Demo12{
    public static void main(String[] args) {
        String s1 = "RAM";
        String s2 = "SITA";
        String s3 = s1 + s2;
        String s4 = s1 + s2;
        if(s3.equals(s4)){
            System.out.println("String's contents are equals");
        }else{
            System.out.println("String's contents are not equals");
        }
    }
}
/*
operand, operator

   assignment
* s2 = s1;
*
*
* */

class Demo13{
    public static void main(String[] args) {
        String s1 = "RAM"; //constant pool //2000
        String s2 = s1; //no object creation just an assignment //2000
        if(s1 == s2){
            System.out.println("Reference are equals");
        }else{
            System.out.println("Reference are  not equals");
        }
    }
}

/*
* compareTo
* */

// it compare charecter by charecter

class Demo14 {
    public static void main(String[] args) {
        /*
         *
         * A = 65
         * B = 66
         * C = 67
         * D = 68
         * E = 69
         * ...
         * J = 74
         *
         *
         * Z = 95
         *
         * a = 96
         *
         * */
        //-1
        String s1 = "Ram";
        String s2 = "Rita";


        int number = s1.compareTo(s2);
        System.out.println("Ram compare to Rita "+ number);

        if (number < 0) {
            System.out.println("s1 is smaller than s2");
        } else if (number > 0) {
            System.out.println("s1 is greater than s2");
        } else {
            System.out.println("s1 is equal to s2");
        }

        String a = "A";
        String b = "E";
        System.out.println("A compare to E: " + b.compareTo(a));
    }
}

    /*
    * in java individual characters of string cannot be access directly like c and c++
    *
    *
    * */

     class Demo15{
        public static void main(String[] args) {
            String a = "ram";
            System.out.println(a);
            //System.out.println(a[1]);
        }
    }

    /*
    * intern()
    *
    * used to take a string present in a non-constant pool to the constant pool
    *
    *
    * but we cannot take the string from constant pool to non-constant pool.
    *
    * there is no method like intern for reversing its activity.
    * */

     class Demo16{
        public static void main(String[] args) {
            String s1 = new String("binod"); //non constant pool
            String s2 = s1.intern(); // constant pool
            System.out.println(s1);
            System.out.println(s2);
            String s3 = "binod"; //constant pool
            if(s2 == s3){
                System.out.println("s2 is in constant pool");
            }else{
                System.out.println("s2 is in non constant pool");
            }
        }
    }


    /*
    * some basic util of string in java
    *
    * */
    class Demo17{
        public static void main(String[] args) {
            String s = "MadanKrishnaKhrestha";
            System.out.println("To upper case => " + s.toUpperCase());
            System.out.println("To lower case => " + s.toLowerCase());
            System.out.println("Contains Krishna " + s.contains("Krishna"));
            System.out.println("Contains Kishna " + s.contains("Kishna"));
            System.out.println("Index of K " + s.indexOf("z"));

            System.out.println("First 5 characters exclude " + s.substring(5));
            System.out.println("From 5th index to 11th character " + s.substring(5,12));

        }
    }

    /*
    * mutable string
    *
    * */

   class Demo18{
       public static void main(String[] args) {
           StringBuffer s = new StringBuffer("Madan"); //non constant pool
           System.out.println(s);
           s.append(" Krishna");
           System.out.println(s);
           /*
           *
           * */
           System.out.println("-------------------------");
           String s1 = "Madan"; //immutable string // constant pool
           System.out.println(s1);
           s1 = s1 + "Krishna";
           System.out.println(s1);
       }
   }

   class Demo19{
       public static void main(String[] args) {
           StringBuilder s = new StringBuilder("Madan");
           System.out.println(s);
           s.append(" Krishna");
           String s1 = s.toString();
           System.out.println(s);
       }
   }

   /*
   * calculation of size
   *
   * Initial size = 16
   * newSize =- oldSize * 2 + 2
   *
   * */

   class Demo20{
       public static void main(String[] args) {
           StringBuilder s = new StringBuilder();
           System.out.println("Capacity of StringBuffer " + s.capacity());
           s.append("Madan Krishna");
           System.out.println(s);
           System.out.println("Capacity of StringBuffer " + s.capacity());
           s.append(" Shrestha");
           /*
           * room capacity = 2*n(old capacity) + 2
           * */
           System.out.println(s);
           System.out.println("Capacity of StringBuffer " + s.capacity());
           s.append(" is a Nepali Comedian actor.");
           System.out.println(s);
           System.out.println("Capacity of StringBuffer "+ s.capacity());
       }
   }


   /*
   * StringTokenizer class is used to chop a string at a specified character
   * and produces strings or tokens
   *
   * */
   class Demo21{
       public static void main(String[] args) {
           StringTokenizer s = new StringTokenizer("Madan Krishna Shrestha", " ");
            //[[madan],[Krishna],[Shrestha]]

           /*
           * retrieve/ access
           *
           * we dont know how many tokens have been created.
           *
           * Its not like accessing array, access through index
           *
           * hasMoreTokens() -> true/false -> Madan, Krishna, Shrestha
           *
           * nextToken() --> return token/substring/chopped string/slited string
           * */

           while(s.hasMoreTokens()){
               System.out.println(s.nextToken()); // madan,krishna, shrestha
           }
       }
   }

   /*
   * Behaviour of System.out.print(string)
   *
   *
   *
   * println(string)
   *
   * valueOf() --> method of string, it converts any primitive(
   * byte,short,int,long,float,double,char,boolean
   * ) data to String object
   *
   *
   *
   *
   * */

class Dog{

}

  class Demo22{
      public static void main(String[] args) {
          Dog dog = new Dog();
          System.out.println("Madan"+ 'K' + 5 + dog.toString());
          char c = 'K';
          String cString = String.valueOf(c);  //"k"
          int a  = 5;
          String aString = String.valueOf(a); // "5"

          boolean b = true;
          String bString = String.valueOf(b);

          int aa[] = new int[2];
          String aaString = aa.toString();

          System.out.println("Madam"+ String.valueOf('K') + String.valueOf(5) + aa.toString());

          /*
          * till now object -> method access
          *
          * String s1 = "ram"; // object create s1
          * s1.toUpperCase();  //method use/access
          *
          *
          * String.valueOf() //static method without object acessible
          *
          * */



          System.out.println(5);
          System.out.println(true);
          System.out.println(0.5f);
          System.out.println('/');
      }
  }

  /*
  * Exception in String --> StringIndexOutOfBounds
  *
  * try to access a character beyond the boundary of
  * the string using the charAt()
  *
  * */
  class Demo23{
      public static void main(String[] args) {
          String s = "Madan";
          char a[] = s.toCharArray();
          /*
          * static method, static variable --> without object can be accessed.
          *
          * */
          //String.valueOf() --> static
          System.out.println(a[2]); // d
          System.out.println(a[6]);  // System --> class, out --> static variable  --> println()
      }
  }


  /*
  *
  * immutable string when new string is concat
  *
  * concatenation really happens but without reference it cannot be displayed
  *
  *
  * new object is created destroying previous object.
  * */

  class Demo24{
      public static void main(String[] args) {
          String s = "Madan";
         // System.out.println(s.concat(" Krishna"));
          s = s.concat(" Krishna");
          System.out.println(s); //madan
      }
  }

  /*
  *
  * split() ,chop, divider, regular expression
  * */
  class Demo25{
      public static void main(String[] args) {
          String s  = "Suraj Gautam";
          char charecterArray[] = s.toCharArray(); //['S','u','r','a','j',' ','G','a','u','t','a','m']
          String splitedArray[] = s.split(" "); // ["Suraj", "Gautam"]
          String s1= "a b";
          s1.toCharArray(); // ['a',' ','b']
          String splitedS1[] = s1.split(" "); //["a","b"]

          splitedArray[0].toCharArray(); //['S','u','r','a','j']

          for(int i =0; i< splitedArray.length; i++){
              System.out.println(splitedArray[i]);
          }
      }
  }
  


  class Demo26{
      public static void main(String[] args) {

      }
  }

  /*
  * jre- > 4 region --> bottom --> heap (object , string--> object) ---> string pool --> cp, ncp
  *
  * */
