package serilization;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;


/*
* marker interface
* imporvised serializable -->
*
* */

public class Player implements Serializable {
     String name;
     int goals;
     float average;
     boolean isNationalPlayer;


    /*
    * using Externalizable(I)
    *     writeExternal(m)
    *     readExternal(m)
    *
    *      class must have empty constructor
    * */
    public Player(){

    }


    public Player(String name, int goals, float average, boolean isNationalPlayer) {
        this.name = name;
        this.goals = goals;
        this.average = average;
        this.isNationalPlayer = isNationalPlayer;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name ='" + name + '\'' +
                ", goals =" + goals +
                ", average =" + average +
                ", isNationalPlayer =" + isNationalPlayer +
                '}';
    }



   /* @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(goals);
        out.writeBoolean(isNationalPlayer);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        goals = in.readInt();
        isNationalPlayer = in.readBoolean();
    }*/


    public void writeObject(ObjectOutputStream oos) throws IOException{
        oos.writeInt(goals);
        oos.writeBoolean(isNationalPlayer);
    }

    public void readObject(ObjectInputStream ois) throws IOException{
        goals = ois.readInt();
        isNationalPlayer = ois.readBoolean();
    }



}

                     //player
//[source]   --------------------------> [destination]


/*
* SHA   --> secure hasing algorithem
*
* UID
*
* */


