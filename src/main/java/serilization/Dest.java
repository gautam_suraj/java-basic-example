package serilization;



import java.io.*;

public class Dest {

    public static void main(String[] args) throws IOException {
        String outputPath = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/player.txt";
        FileInputStream fileInputStream = new FileInputStream(outputPath);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream);

        try {
            Player player =  (Player)objectInputStream.readObject();
            System.out.println("Deserilization ........... ");
            System.out.println(player.toString());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
