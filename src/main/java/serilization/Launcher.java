package serilization;

import java.io.*;

public class Launcher {

    public static void main(String[] args) {
        Player player = new Player("Hari Khadka",50,12.0f,true);
        String outputPath = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/player.txt";
        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;
        BufferedOutputStream bufferedOutputStream = null;

        /*
        * for making less harddisk hit less we need to use BufferedOutputStream
        *
        * */
        try {
            fileOutputStream =  new FileOutputStream(outputPath);
            bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            objectOutputStream =  new ObjectOutputStream(bufferedOutputStream);

            objectOutputStream.writeObject(player);
            bufferedOutputStream.flush();
        } catch (FileNotFoundException e) {   //checked exception
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println(player);
    }

    /*
    * Serlized --> primary mermory object --> persistant
    *
    *
    * */
}



/*
 * can be persistant/stored
 *
 * */

/*
 * marker interface
 * informed
 *
 * */




