package serilization;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class FootballPlayer implements Externalizable {

     private String name;
     private int goals;
    private float average;
    private boolean isNationalPlayer;

    public FootballPlayer(String name, int goals, float average, boolean isNationalPlayer) {
        this.name = name;
        this.goals = goals;
        this.average = average;
        this.isNationalPlayer = isNationalPlayer;
    }

    @Override
    public String toString() {
        return "FootballPlayer{" +
                "name='" + name + '\'' +
                ", goals=" + goals +
                ", average=" + average +
                ", isNationalPlayer=" + isNationalPlayer +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(goals);
        out.writeBoolean(isNationalPlayer);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        goals = in.readInt();
        isNationalPlayer = in.readBoolean();
    }
}
