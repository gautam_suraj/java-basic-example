package collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class SetExample {
    public static void main(String[] args) {

    }

    private void hashSet(){
        /*
        * internally makes use of hash/hashing.
        * Hashing makes the use of hashFunction and hashTable
        *
        * hash function produce  a unique hash key such that collision does not occour
        * load factor of hashtable is fixed as 0.75, i.e when hash table gets filled
        * to an extent of 75% then automatically the table would double its size,
        * reducing the collision.
        *
        * requires a time complexity of O(1) for search operation
        * */


        HashSet hashSet = new HashSet();
        hashSet.add(100);
        hashSet.add(150);
        hashSet.add(24);
        hashSet.add(30);
        hashSet.add(80);
        System.out.println("hash set is: "+ hashSet);

        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add(100);
        linkedHashSet.add(150);
        linkedHashSet.add(24);
        linkedHashSet.add(30);
        linkedHashSet.add(80);
        System.out.println("hash set is: "+ linkedHashSet);


        for(int i =0; i<linkedHashSet.size();i++){
            //no method to access through index
            System.out.println("member of linked hash set "+ i);
        }

        System.out.println("Through for each");
        for (Object obj:linkedHashSet) {
            System.out.println("member of linked hash set: "+ obj);
        }

        System.out.println("Through iterator ");
        Iterator iterator = linkedHashSet.iterator();
        while (iterator.hasNext()){
            System.out.println("memeber of linked list "+ iterator.next());
        }

    }

    private int simpleHasFunction(int i){

        /*
        * 100  --> 1+0+0 = 1 % 10 = 1
        * 150  --> 1+5+0 = 6 % 10 = 6
        * 50  --> 5+0 = 5 % 10 = 5
        * 25  --> 2+5 = 7 % 10 = 7
        *
        * */


        char[] intChar = String.valueOf(i).toCharArray();
        int addition = 0;
        for (int i1 = 0; i1 < intChar.length; i1++) {
          addition += Integer.parseInt(intChar.toString());
        }
        return addition%10;
    }


}
