package collection;

import java.util.*;

public class Limitation   {
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public static void main(String[] args) {
        ArrayList<Integer> collection = new ArrayList(); //solved fixed size
        System.out.println("Size of collection : " + collection.size());

        System.out.println("Is collection empty "+ collection.isEmpty());

        //check given collection is empty or not


        //size of the collection cannot be negative
        // arr
        if(collection.size() == 0 ){
            System.out.println("Collection is empty");
        }

        if(collection.isEmpty()){
            System.out.println("collection is empty");
        }

        collection.add(1);
        /*collection.add("Ram");  //hetrogenous data
        collection.add('!');*/

        for (int i = 0; i < collection.size(); i++) {
              Object obj   = collection.get(i);
               int x = (Integer) obj;
               x = x + 2;
        }

        //


        ArrayList arrayList = new ArrayList();
        arrayList.add(233);
        arrayList.add(243);
        arrayList.add(253);
        arrayList.add(263);
        arrayList.add(273);

        //[233,243,253,263,273] //rear insersertion, retrival

        collection.addAll(arrayList);

        System.out.println("ArryList collection :");
        System.out.println(arrayList);
        System.out.println("Collection : ");
        System.out.println(collection);

        System.out.println("Is ram in the collection "+ collection.contains("Ram")); //boolean --> true/false
        System.out.println("Is shyam in the collection " + collection.contains("Shyam"));

        System.out.println("Is arrylist in the collection "+ collection.containsAll(arrayList)); // check for existance of collection

        ArrayList collection1 = new ArrayList();
        collection1.add("Hari");
        // collection.addAll(collection1);
        System.out.println("Is collection1 in the collection " +collection.containsAll(collection1));

        System.out.println("Size of collection : " + collection.size());


        /*
        * list specific method
        * */

        System.out.println("geting value from list " + collection.get(2));

        System.out.println("getting index of the object in the collection "+ collection.indexOf('!'));

        System.out.println("getting index of the object in the collection "+ collection.indexOf("!"));

        System.out.println("get all values " + collection);
        System.out.println("removing object from collection " + collection.remove(2)); // list
        System.out.println("removing object from collection " + collection.remove("Ram")); // collection
        System.out.println("get all values " + collection);
        //collection.remove('!'); // premitive data type
        //is used in all the collection
        System.out.println("arrylist " + arrayList);
        System.out.println("removing arrylist collection " + collection.removeAll(arrayList));  //collection method
        System.out.println("get all values " + collection);
        collection.add(1);
        //collection.add("Ram");
        //collection.add("Ram");
        System.out.println("Adding again 1 in collection  " + collection);
        System.out.println("getting index of ram " + collection.indexOf("Ram")); // 1
        System.out.println("getting last index of ram " +  collection.lastIndexOf("Ram")); // 4


        /*
        *
        * */
        collection.clear();

        System.out.println("getting all values " + collection );

        //retain

        ArrayList arr1 = new ArrayList();
        ArrayList arr2 = new ArrayList();

        arr1.add(1);
        arr1.add(2);
        arr1.add(3);

        arr2.add(1);
        arr2.add(3);
        arr2.add(5);
        System.out.println("arr1 " + arr1);
        System.out.println("arr2 " + arr2);

        /*arr1.retainAll(arr2);

        System.out.println("arr1 " + arr1);
        System.out.println("arr2 " + arr2);*/

        arr2.retainAll(arr1); // intersection of set --> placing common data in one container --> that container is
                                // same collection in this case container is arr2
        System.out.println("arr1 " + arr1);
        System.out.println("arr2 " + arr2);


        arr1.set(1,5);  // index, element
        //[1,2,3]
        //[1,5,3] // update value at the given index
        System.out.println("get arr1 after setting 5 at index 1 "+ arr1);

        arr1.add(1,5); //shifting operation
        //arr1.get(4);
        arr1.add(5);

        ArrayDeque arrayDeque = new ArrayDeque();
        //front insertion and rear insertion

        arrayDeque.add(2);
        arrayDeque.add(3);
        arrayDeque.add(1);
        arrayDeque.addFirst(4);  //front insertinon
        arrayDeque.addLast(5);// rear insertion

        //[4,3,5]
        System.out.println("geeting " + arrayDeque);
        System.out.println("removing value 2 " + arrayDeque.remove(2));
        System.out.println("geeting " + arrayDeque);
        System.out.println("peek "+ arrayDeque.peek()); // fetch
        System.out.println("poll "+ arrayDeque.poll()); // delete
        System.out.println("geeting "+ arrayDeque);
        System.out.println("peek " + arrayDeque.peek());
        System.out.println("peek " + arrayDeque.peek());
        System.out.println("pick first elelment "+ arrayDeque.peekFirst());
        System.out.println("peek last element "+  arrayDeque.peekLast());

        System.out.println("array deque " + arrayDeque);
        arrayDeque.size();



        arrayDeque.remove();

        //[1,2,3]
        //[1,5,2,3]


        System.out.println("add element 5 at index 1 " + arr1);


        LinkedList linkedList = new LinkedList();
        linkedList.add(10);
        linkedList.add(20);
        linkedList.add(30);
        linkedList.add(40);
        linkedList.add(50);


        arrayList.add(2,25); //shiifing operation
        linkedList.get(3);
        linkedList.remove(new Integer(50));


        //duplicate and insertion order preserved --> list
        //arraylist --> retrival operation as compared to insertion
        //linkedlist --> insertion operation is more as compared to retrival



        //set
        //[100,150,50,25,75]

        //hash function = i % 10

        /*
        * 100 => 1+0+0 = 1 % 10 = 1
        * 150 => 6
        * 50 => 5
        * 25 => 7
        * 75 => 7 + 5 = 12 = 12 % 10 = 2
        *
        * */

       /*
       *  hashtable
       * 1[100]
       * 6[150]
       * 5[50]
       * 7[25]
       *
       *
       * */

        Set set = new HashSet();
        set.add(100);
        set.add(150);
        set.add(50);
        set.add(25);
        set.add(70);
        set.add(25);

        System.out.println("Hash Set " + set);

        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add(100);
        linkedHashSet.add(150);
        linkedHashSet.add(50);
        linkedHashSet.add(25);
        linkedHashSet.add(70);
        linkedHashSet.add(25);
        linkedHashSet.addAll(set);
        arr1.add(17);
        linkedHashSet.addAll(arr1);  //arr1 ==> arraylist
        System.out.println("Linked hash set " + linkedHashSet);
        System.out.println("Arr1 " + arr1);
        arr1.remove(5); // removing 17 // i
        System.out.println("Arr1 " + arr1);

        System.out.println("Linked hash set " + linkedHashSet);

        System.out.println("contains 100 " + set.contains(100));

        set.isEmpty();
        set.remove(100);

        arr1.remove(Integer.valueOf(1));
        linkedList.retainAll(arr1);


        arr1.add(1);   // arr1.add(Integer.valueOf(1));
        // premittive data type -> object ==> BOxing
        //object --> premitive data type ==> Unboxing

        Object i = arr1.get(1);
        int a = (Integer) i;   //automatically unboxing

     /*   ArrayList<Integer> list = new ArrayList();
        list.add(1);
        int i1 = list.get(1);  //automatically unboxing */


       // arr1.contains(1);



      //  set.remove(25); // get and delete
        /*
        * hashCode == 7
        * */




        Queue queue = new PriorityQueue();
        //100,50,150,25,75,125,175
        queue.add(100);
        queue.add(50);
        queue.add(150);
        queue.add(25);
        queue.add(75);
        queue.add(125);
        queue.add(175);

        System.out.println("displaying priority queue " + queue);

        Set<Integer> treeSet = new TreeSet();
        treeSet.add(100);
        treeSet.add(50);
        treeSet.add(150);
        treeSet.add(25);
        treeSet.add(75);
        treeSet.add(125);
        treeSet.add(175);


        System.out.println("displaying tree set" + treeSet);




    }
}
//Jassue colllection framework
//efficient storage, LinkedList
// searching,  --> Set
// sorting   --> Queue
