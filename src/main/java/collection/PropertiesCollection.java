package collection;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class PropertiesCollection {
    /*
    *
    * legacy classes --> vector,stack,hashtable,properties,dictionary
    * */


    //key value
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(14,12);
        properties.put("name","Ram");
        System.out.println(properties);

        Integer integer = (Integer) properties.get(14);
        System.out.println("get value "+ integer);



        /*
        * Array has to changed to list
        *
        * */

        int[] arr = new int[10];
        List list = Arrays.asList(arr);

        //cant list.add
        //cant list.remove
        //cannot affect the size
        //but can update //sort
        //hetrogenous data


    }
}
