package collection;



import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

public class LoopingCollectiion {
    /*
    * concurrent
    *
    * */

    /*


    * fail fast
    * fail safe
    *   structual modificatioin stop
    * */
    public static void main(String[] args) {
        List<Integer> list = new CopyOnWriteArrayList<>();
        list.add(19);
        list.add(19);
        list.add(12);
        list.add(13);
        list.add(13);
        list.add(41);
        list.add(53);

        System.out.println(" list " + list);


        HashSet<Integer> listToSet = new HashSet<>(list);

        System.out.println("List to set " + listToSet);

        list.iterator();
        list.listIterator();

        //Jassue --> COllection Framework // open source 1.2

        /*
        * vector,stack,dictionay, jdk 1.0 --> legcy classes
        * */
        Vector<Integer> vector = new Vector();
        vector.add(1);
        vector.add(2);
        vector.add(3);

        //iteration -- loop
        Enumeration enumeration = vector.elements();
        while (enumeration.hasMoreElements()){
            System.out.println(enumeration.nextElement());
        }

        ArrayList<Integer> setToList = new ArrayList<>(listToSet);
        System.out.println("Set to list " + setToList);

        ListIterator<Integer> iterator = list.listIterator();


        //[1 2 3 4 5]
        while (iterator.hasNext()){
            Integer element = iterator.next();
            System.out.println(element);

        }
        System.out.println("------------------");
        while (iterator.hasPrevious()){
            System.out.println(iterator.previous());
        }

        List<Student> students = new ArrayList<Student>();
        students.add(new Student(1,"Ram"));
        students.add(new Student(32,"Shyam"));
        students.add(new Student(12,"Hari"));

       // Collections.sort(students);
        Collections.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        Collections.sort(students,new StudentComparator());



        System.out.println("Studnets    " + students);



        Collections.sort(list);

        //anynomous class



        //Collections --> Utility Class  // every method in this class are static





        //Comparable and Comparator

        //iterator
        //list iterator

        System.out.println("List " + list);



        Set<Integer> integerSet = new CopyOnWriteArraySet<>();
        integerSet.add(1);
        integerSet.add(2);
        integerSet.add(3);
        integerSet.add(4);
        integerSet.add(5);




        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        //[1,2,3,4,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]

        //concurrent exception  till jdk 1.8
       /* for (Integer integer: list) {
            System.out.println(integer);
            list.add(6);
        }*/


        if(queue.contains(6)){

        }


        /*for (Integer data :integerSet){
            System.out.println(data);
        }*/




    }

    //nested class
    static class Hero{

    }


    /*
    * key, value
    * */
    /**/





}

enum HumanGender{
    MALE,
    FEMALE,
    OTHERS
}




