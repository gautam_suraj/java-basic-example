package collection;

import java.awt.*;
import java.util.*;

public class StudentComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }

    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        map.put("Ram",24);
        map.put("Shyam",28);
        map.put("Hari",14);



        System.out.println("getting locker  of ram "+  map.get("Ram"));

        System.out.println("--- sundar "+map.get("sundar")); //null or value

        if(map.containsKey("sundar")){  //true or false
            System.out.println(map.get("sundar").byteValue());
        }



        //NullPointerException

        System.out.println(" "+ map.containsKey("Hari"));

        map.containsKey("madan");
        map.containsValue(14);

       // map.clear();   //void  --> removes all data from map

        System.out.println("clearing map " + map);

        map.isEmpty();  //check   // true/false

        map.size();  // check the size  // int

        System.out.println("removed hari " + map.remove("Hari"));  // 14 value

        System.out.println("removed sundar " + map.remove("sundar")); //null


        //map.put("Ram", 28);  // if key is already present in the map then it updates the value

        System.out.println("Before putting ram value " + map);

        System.out.println("putting ram value " + map.put("Ram", 28)); //value


        System.out.println("putting sundar value " + map.put("Sundar", 31)); // value

        System.out.println("replacing bhisnu value " +  map.replace("Bhisnu", 38)); //value

        //map.replace("Ram", 38);

        System.out.println("Removing hari " + map);




        //entry set

        //String,Integer

        //Entry is nested interface
        //1000
        Set<Map.Entry<String, Integer>>
                entries = map.entrySet();
        Iterator<Map.Entry<String, Integer>>
                entryIterator = entries.iterator();


        while (entryIterator.hasNext()){
          Map.Entry<String,Integer>
                  entry = entryIterator.next();
         String key =  entry.getKey();
         Integer value =  entry.getValue();
        }






        System.out.println(map);

        System.out.println("get all the keys " + map.keySet());

        System.out.println( "get all values ..." + map.values());

        Collection<Integer> integers = map.values(); //duplicate must allowed

       Set<String> keys = map.keySet();
       Iterator<String>
               iterator = keys.iterator();
       while (iterator.hasNext()){
           String key = iterator.next();
           Integer value = map.get(key);
           System.out.println("key = "+
                   key + " && value = " +
                   value);
       }

       Set<Map.Entry<String,Integer>> entrySet = map.entrySet();


    }
}

interface ABC{
    interface XYZ {

    }
}
