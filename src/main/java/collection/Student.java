package collection;


public class Student implements Comparable<Student>{
    int rollNo;
    String name;
    Student(int rollNo,String name){
        this.rollNo = rollNo;
        this.name = name;
    }


    @Override
    public String toString() {
        return "Student{" +
                "rollNo=" + rollNo +
                ", name='" + name + '\'' +
                '}';
    }




    @Override
    public int compareTo(Student o) {
        return this.name.compareTo(o.name);
    }
}
