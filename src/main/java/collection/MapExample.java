package collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class MapExample {

    public static void main(String[] args) {

        HashMap map = new HashMap();
        map.put(1,100);
        map.put(2,200);
        map.put(3,300);
        map.put(4,400);
        map.put(5,500);


        System.out.println("Map contains : "+ map);

        /*
        * keySet() returns in Set
        * */
        System.out.println("Get all keys of map: "+ map.keySet());

        /*
        * returns in collection...
        *
        * */
        System.out.println("Get all values of map: "+ map.values());


        System.out.println("Getting value of 4 "+ map.get(4) );


        System.out.println("Does map contains key 2: "+ map.containsKey(2));


        System.out.println("Does map contains value 400: "+ map.containsValue(400));


        map.put(2,2000);

        System.out.println("map values "+ map);

        /*
        *
        *
        *
        * */

      Set entrySet =  map.entrySet();
      Iterator entrySetIterator = entrySet.iterator();
      while (entrySetIterator.hasNext()){
          System.out.println("Items of entry Set...." + entrySetIterator.next());
      }


    }


}
