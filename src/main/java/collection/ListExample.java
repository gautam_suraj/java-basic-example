package collection;

import java.util.*;

public class ListExample {

    public static void main(String[] args) {

    }

    private void addDemo(){
        //uses dynamic array
        ArrayList a1 = new ArrayList();
        a1.add(10);
        a1.add(20);
        a1.add(30);
        a1.add(40);
        a1.add(50);
        System.out.println(a1);

        ArrayList a2 = new ArrayList();
        a2.add(100);
        a2.add(200);
        a2.add(300);
        a2.add(400);
        a2.add(500);
        System.out.println(a2);

        a1.addAll(a2);
        System.out.println(a1);

        ArrayList a3 = new ArrayList();
        a3.add(1000);
        a3.add(2000);
        a3.add(3000);

        ArrayList a4 = new ArrayList();

        System.out.println("size of a1: "+ a1.size());
        System.out.println("a1 contains 100: "+ a1.contains(100));
        System.out.println("a1 contains 1000: "+ a1.contains(1000));

        System.out.println("a1 contains a2: "+  a1.containsAll(a2));
        System.out.println("a1 contains a3: "+  a1.containsAll(a3));

        System.out.println("get item at postion 2" + a1.get(2));
        System.out.println("a1 class : "+ a1.getClass());

        System.out.println("get index of 40 "+ a1.indexOf(40));
        System.out.println("get index of 60 "+ a1.indexOf(60));

        System.out.println("Is a4 empty "+ a4.isEmpty());

        a4.add(40);
        a4.add(20);

        a1.retainAll(a4);

        System.out.println("Retaining a1 with a4 : "+ a1);

        System.out.println("data of a4 "+ a4);
        a4.add(1, 10);
        System.out.println("data of a4 "+ a4);
        a4.set(2,15);
        System.out.println("data of a4 "+ a4);




    }

    private void executeListIterator(List list){
        ListIterator listIterator = list.listIterator();
        //ListIterator listIterator = list.listIterator(2);
        System.out.println("Moving forward....");
        while(listIterator.hasNext()){
            System.out.println(listIterator.next());
        }

        System.out.println("Moving backward direction .... ");
        while(listIterator.hasPrevious()){
            System.out.println(listIterator.previous());
        }


    }

    private void extraFuction(){
        ArrayList a1 = new ArrayList();
        System.out.println("size of a1: "+ a1.size());
        a1.ensureCapacity(5);
        a1.add(1);
        a1.add(2);
        a1.add(3);
        System.out.println("size of a1: "+ a1.size());
        a1.trimToSize();
        System.out.println("treaming to size "+ a1.size() );
    }

    private void arrayDeque(){
        ArrayDeque arrayDeque = new ArrayDeque();
    }

    private void linkedList(){
        //uses doubly liniked list
        LinkedList ll = new LinkedList();
        ll.add(10);
        ll.add(20);
        ll.addFirst(30);
        System.out.println(ll);
        ll.addLast(10);
        System.out.println(ll);
        ll.push(10);
        System.out.println(ll);
        ll.pop();
        System.out.println(ll);

        //offer vs add
    }
}
