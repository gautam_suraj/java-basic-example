package io;

import java.io.*;

public class IOThroughFileReader {
    public static void main(String[] args) {
        String inputPath = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/test.txt";
        String path = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/text.txt";
        String outputPath = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/outputFile.txt";




        // hdd      -- FileInputStream  //pipe    -->          ram   //when i am trying to read a file
        // ram      -- FileOutputStream //pipe   -->          hdd    //when i am trying to write in a file


        // hdd      -- FileReader  //pipe    -->          ram   //when i am trying to read a file
        // ram      -- FileWriter //pipe   -->          hdd    //when i am trying to write in a file


        //usage internal storage
        //inputPath only read --->  store in a internal storage(1024 bytes) --> when it get fully filled
        // --> then only it will write in outputpath




        /*
        * general idea
        *
        *
        * */

        File file = new File(inputPath);
        System.out.println("Is file exist " + file.exists());

        if(file.exists()){
            if(file.isFile()){
                if(file.canExecute()){
                    file.getAbsoluteFile();
                    file.getName();
                    file.getParentFile();
                }
            }
        }




        FileReader fileReader = null;
        FileWriter fileWriter = null;
        BufferedReader bufferedReader =  null;
        BufferedWriter bufferedWriter = null;

        try {
             fileReader = new FileReader(inputPath); //crate input pipe
             fileWriter = new FileWriter(outputPath); //create output pipe

             bufferedReader = new BufferedReader(fileReader);
             bufferedWriter = new BufferedWriter(fileWriter);

            int temp;
            while ((temp = bufferedReader.read()) != -1){   //-1 end of file
               // System.out.println((char) temp);
                bufferedWriter.write(temp);
            }
            fileWriter.flush();  // it will flush internal storage
            //flushing internal storage is writing the contents of internal storage to the file(output file)
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            //closing statement
            //freeing the resources

            try {
                bufferedReader.close();
                bufferedWriter.close();
                fileReader.close();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}
