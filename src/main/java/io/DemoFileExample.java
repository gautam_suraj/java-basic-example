package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/*
* package
* java.lang.* ==> we dont need to import // default package
*
* java.utils.*  ==>
*
*
*
*
* */

public class DemoFileExample {
    public static void main(String[] args) {
        //file access
        //file path
        //absolute path
        String inputPath = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/test.txt";
        String outputPath = "/Users/surajgautam/texas/basic_java/java-basic-example/src/main/resources/outputFile.txt";


        // hdd      -- FileInputStream  //pipe    -->          ram   //when i am trying to read a file
        // ram      -- FileOutputStream //pipe   -->          hdd    //when i am trying to write in a file



        try {
            FileInputStream fileInputStream = new FileInputStream(inputPath); //crate input pipe
            FileOutputStream fileOutputStream = new FileOutputStream(outputPath); //create output pipe

            int temp;
            while ((temp = fileInputStream.read()) != -1){   //-1 end of file
               fileOutputStream.write(temp);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


/*
* file is alwz in hdd
* if we want to read a file i.e we will be reading the content of the file
* our program is in RAM
*
* 1. we need to create a pipe for the transmission of stream of data (content of the file )
*     betweeen File -- RAM
*
* 2.
*
*
* */
